/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/03 17:42:39 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 13:54:02 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include "libft.h"
# include <fcntl.h>

/*
**	T_CELL STRUCTURE
**	structure qui contiendra toutes les salles enregistrer par le parser
**		- index:			servira a travailler avec la salle afin de
**								la retrouver plus= plus rapidement
**		- nb_of_related		indique le nombre de salle associées afin
**								de parcourir le tableau d'int
*/
typedef struct			s_cell
{
	ssize_t				index;
	int					visited;
	char				*name;
	struct s_cell		*next;
}						t_cell;

/*
**	T_PATH STRUCTURE
**	cette structure contiendra tous les paths enregistrés par le BFS
**		- ant_index:	index de la fourmie presente dans la cellule
**		- cell:			adresse de la cellule enregistrées dans le path
*/
typedef struct			s_path
{
	int					ant_index;
	t_cell				*cell;
	struct s_path		*next;
}						t_path;

/*
**	T_MULPATH STRUCTURE
**	cette structure contient tous les differentes chemins après de multiples
**	appels au BFS
**		- index:		index of the path
**		- length:		length of the path
**		- path:			address of the first cell of the path
*/
typedef struct			s_mulpath
{
	int					index;
	int					ants;
	int					length;
	t_path				*path;
	struct s_mulpath	*next;
}						t_mulpath;

/*
**	T_SUPER	STRUCTURE
**	ONE STRUCTURE TO RULE THEM ALL
**		- mul:			Address of mulpath
**		- next:			Da list is a chained list. Duh
*/
typedef struct			s_super
{
	t_mulpath			*mul;
	struct s_super		*next;
}						t_super;

/*
**	T_MATRIX STRUCTURE
**	cette structure contient la matrice de capacite et la matrice de capacite
**	residuelle
**		- residual:		matrice de capacite residuelle
*/
typedef struct			s_matrix
{
	int					*path;
	int					**map;
	int					**residual;
	int					len;
}						t_matrix;

typedef struct			s_ant
{
	int					name;
	t_path				*path;
	struct s_ant		*next;
}						t_ant;

typedef struct			s_all
{
	t_matrix			**matrices;
	t_cell				**cells;
	t_path				**path;
	t_mulpath			**paths;
}						t_all;

/*
**	PRINT TOOLS
**	lem_in.c:
**		- ft_error
**	print_tools.c:
**		- print_error
**		- print_cells
**		- print_paths
*/
void					ft_error(int error);
int						print_error(void);
void					print_cells(t_cell *cells);
void					print_paths(t_path *paths);
void					print_matrice(int **matrice, int len);

/*
**	CELLS TOOLS
**	cells_tools.c
**		- initialize_cell
**		- cell_length
**		- push_back_cell
**		- get_cell
**		- copy_path
**	clean_tools.c
**		- clean_cells
**		- clean_paths
**		- clean_matrix
*/
t_cell					*initialize_cell(int index, char *name);
int						cell_length(t_cell *cells);
void					push_back_cell(t_cell **cells, int index, char *name);
t_cell					*get_cell(t_cell **cells, int index);
t_path					*copy_path(t_path **src);
void					clean_cells(t_cell **cells);
void					clean_paths(t_path **paths);
void					clean_mulpath(t_mulpath **paths);
void					clean_matrix(t_matrix **matrices);
void					ft_stomp(t_ant **ant);

/*
**	CHECK TOOLS
**	check_tools.c
**		- ft_check_map
**		- ft_parse_link
*/
void					ft_check_map(char *str);
void					ft_parse_link(char *str, t_cell **cells,
	int ***map, int size);
void					check_cells(t_cell **cells);

/*
**	PARSE TOOLS
**	parse_tools.c
**		- ft_parse_map
*/
int						ft_parse_map(t_cell **cells, int ***map);

/*
**	MATRICE TOOLS
**	matrix.c
**		- get_path_from_matrix
**	matrix_tools.c
**		- initialize_matrices
**		- create_matrice
*/
void					init_path_array(int **path, int len, int value);
void					get_path_from_matrix(t_all all, int start,
	int limits[4]);
t_matrix				*initialize_matrices(t_cell **cells, int ***map,
	int *len);
int						**create_matrice(int len);

/*
**	EDMONDS KARP
**	edmonds_karp.c
**		- ek
**	bfs.c
**		- bfsek
**		- after_bfs
*/

int						max_len_mul(t_mulpath **paths);
int						get_flow_mul(t_mulpath **paths);
void					clean_and_push(t_mulpath **paths, t_path **save,
	int end);
int						ek(t_cell **cells, t_super **super,
	int ***map, int limits[4]);
int						bfsek(t_cell **cells, t_matrix **matrices,
	int limits[4]);
void					after_bfs(t_matrix **matrices, int start, int end);

/*
**	PATH_TOOLS
**	path_tools.c
**		- initialize_path
**		- path_length
**		- push_back_path
**		- push_front_path
**		- pop_front_path
*/
t_path					*initialize_path(t_cell *cell);
int						path_length(t_path *path);
void					push_back_path(t_path **path, t_cell *cell);
void					push_front_path(t_path **path, t_cell *cell);
void					pop_front_path(t_path **path);

/*
**	MULPATH TOOLS
**	mulpath_tools.c
**		- print_mulpath
**		- push_back_mulpath
**		- initialize_mulpath
*/
void					print_mulpath(t_mulpath *paths);
t_mulpath				*initialize_mulpath(t_path *path, int index);
void					push_back_mulpath(t_mulpath **paths, t_path *path);

/*
**	SUPER TOOLS
**	super_tools.c
**	- initialize_super
**	- push_back_super
**	- super_print
*/
t_super					*initialize_super(t_mulpath *mul);
void					push_back_super(t_super **super, t_mulpath *mul);
void					super_print(t_super *super);
void					clean_super(t_super **super);

/*
**	ANTS
**	ants.c
**		- ants
*/
void					choose_super(t_super **super, int ants);
t_path					*ft_select_path(t_mulpath **paths,
	int pathi, int *newpath);
float					ft_best_score(t_mulpath **paths, int ants, int size);
void					clean_mulpaths(t_mulpath **paths, int ants, int size,
	int *s);
int						ft_nb_paths(t_mulpath **paths);
void					depart_ants(t_mulpath **paths, int ants);
t_ant					*ft_adds_ant(t_mulpath *tmp, t_ant *tmp2);

/*
**	VISUALIZER
**	visu.c
**		- create_visu_cells
**	visu_tool.c
**		- is_in_path
**		- is_path
**		- write_cells_json
*/
int						is_in_path(t_cell *cells, t_super **super);
int						is_path(t_cell **cells, int i, int j, t_super **super);
void					write_cells_json(t_cell **cells, int i, int j, int fd);
void					create_visu_cells(t_cell **cells, int ***map,
	t_super **super);

#endif
