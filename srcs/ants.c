/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/23 16:18:54 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 16:47:14 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		ft_nb_paths(t_mulpath **paths)
{
	t_mulpath	*tmp;
	int			i;

	i = 0;
	if (paths && *paths)
	{
		tmp = *paths;
		while (tmp)
		{
			tmp = tmp->next;
			i++;
		}
		if (i == 0)
			ft_error(5);
		return (i);
	}
	return (0);
}

int		ft_push_ant(t_ant **ant)
{
	t_ant *tmp;

	tmp = *ant;
	if (!tmp)
		return (0);
	while (tmp)
	{
		if (tmp->path)
		{
			ft_printf("L%d-%s", tmp->name, tmp->path->cell->name);
			tmp->path = tmp->path->next;
			if (tmp->next)
				ft_printf(" ");
		}
		tmp = tmp->next;
	}
	ft_printf("\n");
	ft_stomp(ant);
	return (1);
}

void	ft_add_ant(t_ant **ant, t_mulpath **paths)
{
	t_mulpath	*tmp;
	t_ant		*tmp2;

	tmp = *paths;
	while (tmp)
	{
		if (tmp->ants > 0)
		{
			tmp2 = *ant;
			while (tmp2 && tmp2->next)
				tmp2 = tmp2->next;
			tmp->ants = tmp->ants - 1;
			if (tmp2)
				tmp2->next = ft_adds_ant(tmp, tmp2);
			else
				*ant = ft_adds_ant(tmp, tmp2);
		}
		tmp = tmp->next;
	}
}

void	rohans_charge(t_mulpath **paths, int ants)
{
	t_ant	*ant;
	int		ret;

	ant = NULL;
	ret = 1;
	depart_ants(paths, ants);
	while (ret != 0)
	{
		if ((*paths)->length == 1)
			while (ants-- > 0)
				ft_add_ant(&ant, paths);
		else
			ft_add_ant(&ant, paths);
		ret = ft_push_ant(&ant);
	}
}

void	choose_super(t_super **bonne, int ants)
{
	float		shortest;
	int			size;
	t_super		*tmp;
	t_mulpath	**path;

	tmp = *bonne;
	path = NULL;
	shortest = 0;
	while (tmp)
	{
		size = ft_nb_paths(&(tmp->mul));
		if (shortest == 0)
		{
			shortest = ft_best_score(&(tmp->mul), ants, size);
			path = &(tmp->mul);
		}
		else if (shortest >= ft_best_score(&(tmp->mul), ants, size))
		{
			shortest = ft_best_score(&(tmp->mul), ants, size);
			path = &(tmp->mul);
		}
		tmp = tmp->next;
	}
	if (*bonne)
		rohans_charge(path, ants);
}
