/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bfs.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/07 13:40:32 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 12:57:35 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	check_target(t_matrix **matrices, int i, int from)
{
	int	j;
	int	minus;
	int	pos;

	j = -1;
	minus = 0;
	pos = 0;
	while (++j < (*matrices)->len)
	{
		if ((*matrices)->map[i][j] == 1
			&& (*matrices)->path[i] == -1 && j != from)
		{
			if ((*matrices)->residual[i][j] == -1)
				minus = j;
			else if ((*matrices)->residual[i][j] == 1)
				pos++;
		}
	}
	if ((*matrices)->residual[from][i] == 0 && pos && minus)
		return (minus);
	if (((*matrices)->residual[from][i] == -1)
		|| ((*matrices)->residual[from][i] == 0 && !pos))
		return (1);
	return (0);
}

static int	going_up(t_matrix **matrices, int i[2], t_cell **cells,
	t_path **queue)
{
	int	haveto;

	haveto = check_target(matrices, i[0], i[1]);
	if (haveto == 1)
	{
		(*matrices)->path[i[0]] = i[1];
		if (i[0] != 1)
			push_back_path(queue, get_cell(cells, i[0]));
		else
			return (1);
	}
	else if (haveto > 1)
	{
		(*matrices)->path[i[0]] = i[1];
		(*matrices)->path[haveto] = i[0];
		push_back_path(queue, get_cell(cells, haveto));
	}
	return (0);
}

static int	fill_capacity(t_matrix **matrices, t_path **queue,
	t_cell **cells)
{
	int		i[2];
	t_cell	*w;

	w = (*queue)->cell;
	i[1] = w->index;
	pop_front_path(queue);
	i[0] = -1;
	while (++i[0] < (*matrices)->len)
		if ((*matrices)->map[w->index][i[0]] == 1
			&& (*matrices)->path[i[0]] == -1)
			if (going_up(matrices, i, cells, queue) == 1)
				return (1);
	return (0);
}

int			bfsek(t_cell **cells, t_matrix **matrices,
	int limits[4])
{
	int		i;
	t_path	*queue;

	i = -1;
	while (++i < limits[3])
		(*matrices)->path[i] = -1;
	queue = initialize_path(get_cell(cells, limits[0]));
	while (queue)
	{
		if (fill_capacity(matrices, &queue, cells) == 1)
		{
			clean_paths(&queue);
			return (1);
		}
	}
	return (0);
}

void		after_bfs(t_matrix **matrices, int start, int end)
{
	int	u;
	int	v;

	v = end;
	while (v != start)
	{
		u = (*matrices)->path[v];
		(*matrices)->residual[u][v]++;
		(*matrices)->residual[v][u]--;
		v = u;
	}
}
