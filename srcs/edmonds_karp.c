/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   edmonds_karp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 12:44:24 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 18:32:35 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	order_mul(t_mulpath **paths)
{
	int			i;
	t_mulpath	*tmp;
	t_path		*tmpp;

	tmp = *paths;
	while (tmp)
	{
		if (tmp && tmp->next && tmp->length > tmp->next->length)
		{
			tmpp = tmp->path;
			i = tmp->length;
			tmp->length = tmp->next->length;
			tmp->path = tmp->next->path;
			tmp->next->path = tmpp;
			tmp->next->length = i;
			tmp = *paths;
		}
		else
			tmp = tmp->next;
	}
}

void	push_tmp(int curtarmem[3], t_path **tmp, t_all *all, int *i)
{
	while ((*i)++ < (*all->matrices)->len)
	{
		if (*i < (*all->matrices)->len
			&& (*all->matrices)->residual[curtarmem[0]][*i] == 1
			&& *i != curtarmem[0])
		{
			curtarmem[2] = curtarmem[0] == 0 ? *i + 1 : curtarmem[2];
			push_back_path(tmp, get_cell(all->cells, *i));
			curtarmem[1] = *i;
			break ;
		}
	}
}

void	push(t_super **super, t_all *all)
{
	int			i;
	int			curtarmem[3];
	t_mulpath	*new;
	t_path		*tmp;

	new = NULL;
	tmp = NULL;
	curtarmem[0] = 0;
	i = -1;
	while (curtarmem[0] != 1)
	{
		curtarmem[1] = 0;
		push_tmp(curtarmem, &tmp, all, &i);
		curtarmem[1] == 1 ? push_back_mulpath(&new, tmp) : 0;
		curtarmem[0] = curtarmem[1] == 0 ? 1 : curtarmem[1];
		i = curtarmem[1] == 1 && curtarmem[2] != 0 ? curtarmem[2] - 1 : -1;
		if (curtarmem[1] == 1 && curtarmem[2] != 0)
		{
			curtarmem[0] = 0;
			tmp = NULL;
		}
	}
	order_mul(&new);
	push_back_super(super, new);
}

int		ek(t_cell **cells, t_super **super, int ***map, int limits[4])
{
	int			flow;
	t_matrix	*matrices;
	t_all		all;
	int			ret;

	flow = 0;
	matrices = initialize_matrices(cells, map, &limits[3]);
	all.matrices = &matrices;
	all.cells = cells;
	while (1)
	{
		ret = bfsek(cells, &matrices, limits);
		if (!ret)
			break ;
		flow++;
		after_bfs(&matrices, limits[0], limits[1]);
		push(super, &all);
	}
	clean_matrix(&matrices);
	return (flow);
}
