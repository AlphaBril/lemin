/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/05 14:06:29 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 17:20:21 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void	clean_map(int ***map, int len)
{
	int	i;

	i = -1;
	while (++i < len)
		free((*map)[i]);
	free((*map)[i]);
	free(*map);
}

int		main(int ac, char **av)
{
	int			limits[4];
	int			**map;
	t_cell		*cells;
	t_super		*super;

	cells = NULL;
	limits[0] = 0;
	limits[1] = 1;
	limits[2] = ft_parse_map(&cells, &map);
	super = NULL;
	ek(&cells, &super, &map, limits);
	choose_super(&super, limits[2]);
	if (ac > 1 && !ft_strcmp(av[1], "-v"))
		create_visu_cells(&cells, &map, &super);
	clean_map(&map, limits[3]);
	clean_cells(&cells);
	clean_super(&super);
	return (0);
}
