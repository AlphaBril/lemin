/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mulpath_tools.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/23 11:55:18 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/23 17:21:18 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		print_mulpath(t_mulpath *paths)
{
	t_mulpath	*tmpm;
	t_path		*tmpp;

	tmpm = paths;
	while (tmpm)
	{
		tmpp = tmpm->path;
		ft_printf("%wnew path:\n\tindex:  {yellow}%3d{eoc}\n", 2, tmpm->index);
		ft_printf("%w\tlength: {yellow}%3d{eoc}\n\t", 2, tmpm->length);
		ft_printf("%w\tants: {yellow}%3d{eoc}\n\t", 2, tmpm->ants);
		while (tmpp)
		{
			ft_printf("%w{yellow}[{green}%s{eoc} ({red}%d{eoc}){yellow}]{eoc}",
				2, tmpp->cell->name, tmpp->cell->index);
			if (tmpp->next)
				ft_printf("%w--", 2);
			else
				ft_printf("%w\n", 2);
			tmpp = tmpp->next;
		}
		tmpm = tmpm->next;
	}
}

t_mulpath	*initialize_mulpath(t_path *path, int index)
{
	t_mulpath	*new;

	if (!(new = (t_mulpath*)malloc(sizeof(t_mulpath))))
		return (NULL);
	new->index = index;
	new->length = path_length(path);
	new->path = path;
	new->next = NULL;
	return (new);
}

void		push_back_mulpath(t_mulpath **paths, t_path *path)
{
	t_mulpath	*new;
	t_mulpath	*tmp;

	if (!paths || !*paths)
	{
		*paths = initialize_mulpath(path, 0);
		return ;
	}
	tmp = *paths;
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = initialize_mulpath(path, tmp->index + 1);
	tmp->next = new;
}
