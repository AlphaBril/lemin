/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/18 14:14:34 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/29 17:43:27 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		ft_parse_cell(char *str, t_cell **cells, int *start, int index)
{
	char	**tab;
	char	*line;
	int		i;
	int		j;

	i = 0;
	while (str[i] && str[i] != '\n')
		i++;
	line = ft_strsub(str, 0, i);
	tab = ft_strsplit(line, ' ');
	ft_strdel(&line);
	j = 0;
	while (tab && tab[j])
		j++;
	if (*start == -1)
		index = 1;
	if (*start == 1)
		index = 0;
	if (j == 1)
		*start = -5;
	else if (tab)
		push_back_cell(cells, index, tab[0]);
	ft_tabdel(&tab);
	return (*start == -5 ? 1 : i);
}

int		ft_parse_room(char *str, t_cell **cells)
{
	int		index;
	int		start;
	int		i;

	i = -1;
	index = str[0] == '#' ? 2 : 1;
	while (str[++i])
	{
		start = 0;
		index = str[i] != '#' ? index + 1 : index;
		while (str[i] && str[i] == '#')
		{
			start = ft_strncmp(&str[i], "##start", 7) == 0 ? 1 : start;
			start = ft_strncmp(&str[i], "##end", 5) == 0 ? -1 : start;
			while (str[i] && str[i] != '\n')
				i++;
			i++;
		}
		i = i + ft_parse_cell(&str[i], cells, &start, index);
		if (start == -5)
			return (i);
	}
	return (i);
}

void	ft_swallow(int fd, char **line, char *verif)
{
	char	buf[4092 + 1];
	int		check;
	int		rd;

	check = 0;
	rd = 1;
	while (rd != 0)
	{
		rd = read(fd, buf, 4092);
		if (rd == -1)
			ft_error(0);
		buf[rd] = '\0';
		if (!(*line = ft_strjoin_free(*line, buf, 1)))
			ft_error(100);
		if (verif && ft_match(buf, verif) == 0)
			check++;
		if (check >= 2000)
			ft_error(0);
	}
}

int		ft_parse_map(t_cell **cells, int ***map)
{
	char	*str;
	long	ret;
	int		i;

	str = NULL;
	ft_swallow(0, &str, "*##start*");
	ft_check_map(str);
	ret = ft_atol(str);
	if (ret > 2147483647 || ret < -2147483648)
		ft_error(1);
	i = 0;
	while (str[i] && str[i] != '\n')
		i++;
	i = ft_parse_room(&str[i + 1], cells);
	while (str[i] && str[i] != '\n')
		i++;
	check_cells(cells);
	ft_parse_link(&str[i], cells, map, cell_length(*cells));
	ft_printf("%s\n", str);
	ft_strdel(&str);
	return (ret);
}
