/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/15 13:30:01 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 17:17:48 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_path	*initialize_path(t_cell *cell)
{
	t_path	*new;

	if (!(new = (t_path*)malloc(sizeof(t_path))))
		return (NULL);
	new->ant_index = 0;
	new->cell = cell;
	new->next = NULL;
	return (new);
}

int		path_length(t_path *path)
{
	int		i;
	t_path	*tmp;

	i = 0;
	tmp = path;
	while (tmp)
	{
		i++;
		tmp = tmp->next;
	}
	return (i);
}

void	push_back_path(t_path **path, t_cell *cell)
{
	t_path	*new;
	t_path	*tmp;

	if (!path || !*path)
	{
		*path = initialize_path(cell);
		return ;
	}
	tmp = *path;
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = initialize_path(cell);
	tmp->next = new;
}

void	push_front_path(t_path **path, t_cell *cell)
{
	t_path	*new;

	if (!path || !*path)
	{
		*path = initialize_path(cell);
		return ;
	}
	new = initialize_path(cell);
	new->next = *path;
	*path = new;
}

void	pop_front_path(t_path **path)
{
	t_path	*tmp;

	tmp = *path;
	*path = (*path)->next;
	free(tmp);
}
