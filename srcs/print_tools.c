/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/08 10:14:01 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 17:20:38 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

void		print_matrice(int **matrice, int len)
{
	int	i;
	int	j;

	i = -1;
	ft_printf("    ");
	while (++i < len)
		ft_printf("%d    ", i);
	ft_printf("\n");
	i = -1;
	while (++i < len)
	{
		j = -1;
		ft_printf("%d ", i);
		while (++j < len)
		{
			if (matrice[i][j] < 0)
				ft_printf("{red}[%2d]{eoc} ", matrice[i][j]);
			else if (matrice[i][j] > 0)
				ft_printf("{green}{bold}[%2d]{eoc} ", matrice[i][j]);
			else
				ft_printf("[%2d] ", matrice[i][j]);
		}
		ft_printf("\n");
	}
}

void		ft_error(int error)
{
	if (error == 0)
		ft_printf("Read error %C\n", L'😡');
	if (error == 1)
		ft_printf("Wrong number of ants {inverted} %C  {eoc}\n", L'🐜');
	if (error == 2)
		ft_printf("Wrong number of start/end in the map %C\n", L'🎲');
	if (error == 3)
		ft_printf("Double cells %S\n", L"👯‍");
	if (error == 4)
		ft_printf("Wrong coordinates %C\n", L'🙃');
	if (error == 5)
		ft_printf("No path found %C\n", L'😥');
	if (error == 100)
		ft_printf("Malloc error %C\n", L'💩');
	exit(0);
}

static void	print_cells_data(t_cell *tmp, int i)
{
	ft_printf("{green}t_cell{red}%5d{eoc}\n{\n", i);
	ft_printf("\t{green}%-5s{yellow}%-7s{eoc}%10d\n",
			"int", "index", tmp->index);
	ft_printf("\t{green}%-5s{yellow}%-7s{eoc}%10s\n",
			"char", "*name", tmp->name);
	ft_printf("\t{green}%-5s{yellow}%-7s{eoc}%10d\n",
			"int", "visited", tmp->visited);
	ft_printf("}\n\n");
}

void		print_cells(t_cell *cells)
{
	int		i;
	t_cell	*tmp;

	i = 0;
	tmp = cells;
	ft_printf("{green}%T{eoc}", "cells");
	while (tmp)
	{
		print_cells_data(tmp, i);
		tmp = tmp->next;
		i++;
	}
}

void		print_paths(t_path *paths)
{
	int		i;
	t_path	*tmp;

	tmp = paths;
	ft_printf("%w{green}%T{eoc}", 2, "path");
	i = 0;
	while (tmp)
	{
		ft_printf("%w%-10s%10s\n", 2, "name", tmp->cell->name);
		ft_printf("%w%-10s%10d\n", 2, "index", tmp->cell->index);
		ft_printf("%w\n", 2);
		tmp = tmp->next;
		i++;
	}
}
