/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/14 16:35:41 by fldoucet          #+#    #+#             */
/*   Updated: 2019/05/29 13:03:52 by fldoucet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>

void	clean_mulpaths(t_mulpath **paths, int ants, int size, int *s)
{
	t_mulpath *tmp;
	t_mulpath *prev;

	while (size > 1 && (ft_best_score(paths, ants, size - 1)
			< ft_best_score(paths, ants, size)))
	{
		tmp = *paths;
		prev = NULL;
		while (tmp->next)
		{
			prev = tmp;
			tmp = tmp->next;
		}
		if (prev)
		{
			free(tmp);
			prev->next = NULL;
		}
		*s = ft_nb_paths(paths);
	}
}

t_ant	*ft_adds_ant(t_mulpath *tmp, t_ant *tmp2)
{
	t_ant *new;

	new = NULL;
	if (!(new = malloc(sizeof(t_ant))))
		ft_error(100);
	new->next = NULL;
	new->path = tmp->path;
	if (tmp2)
		new->name = tmp2->name + 1;
	else
		new->name = 1;
	return (new);
}

int		ft_init_path(t_mulpath **paths)
{
	t_mulpath *tmp;

	tmp = *paths;
	while (tmp)
	{
		tmp->ants = 0;
		tmp = tmp->next;
	}
	return (0);
}

void	depart_ants(t_mulpath **paths, int ants)
{
	t_mulpath	*tmp;
	t_mulpath	*last;
	int			i;
	int			j;

	i = ft_init_path(paths);
	last = *paths;
	while (ants > 0)
	{
		tmp = *paths;
		j = 0;
		while (last->next && last->ants >= (last->next->length - last->length))
		{
			last = last->next;
			i++;
		}
		while (tmp && j <= i && ants > 0)
		{
			tmp->ants = tmp->ants + 1;
			ants--;
			j++;
			tmp = tmp->next;
		}
	}
}

float	ft_best_score(t_mulpath **paths, int ants, int size)
{
	t_mulpath	*tmp;
	float		biggest;
	float		i;
	float		ret;

	i = 0;
	tmp = *paths;
	biggest = 0;
	while (tmp && i < size)
	{
		biggest = biggest + tmp->length;
		tmp = tmp->next;
		i++;
	}
	ret = ((ants - 1) / i) + (biggest / i);
	return (ret);
}
