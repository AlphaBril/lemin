/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   super_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 10:35:20 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/29 12:42:55 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_super	*initialize_super(t_mulpath *mul)
{
	t_super	*new;

	if (!(new = (t_super*)malloc(sizeof(t_super))))
		return (NULL);
	new->mul = mul;
	new->next = NULL;
	return (new);
}

void	push_back_super(t_super **super, t_mulpath *mul)
{
	t_super	*new;
	t_super	*tmp;

	if (!super || !*super)
	{
		*super = initialize_super(mul);
		return ;
	}
	tmp = *super;
	while (tmp && tmp->next)
		tmp = tmp->next;
	new = initialize_super(mul);
	tmp->next = new;
}

void	super_print(t_super *super)
{
	int		i;
	t_super	*tmp;

	i = 0;
	tmp = super;
	ft_printf("%T", "super");
	while (tmp)
	{
		ft_printf("\n%w{bold}{red}Super number %d{eoc}\n", 2, i++);
		print_mulpath(tmp->mul);
		tmp = tmp->next;
	}
}

void	clean_super(t_super **super)
{
	t_super	*tmp;

	while (*super)
	{
		tmp = *super;
		*super = (*super)->next;
		clean_mulpath(&tmp->mul);
		free(tmp);
	}
}
