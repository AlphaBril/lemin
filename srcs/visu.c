/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/09 15:45:40 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/21 13:38:47 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void		create_visu_links(t_cell **cells, int ***map, int fd,
	t_super **super)
{
	int	i;
	int	j;
	int	len;
	int	is_first;

	i = -1;
	len = cell_length(*cells);
	is_first = 1;
	ft_printf("\t\"links\": [\n%w", fd);
	while (++i < len)
	{
		j = -1;
		while (++j < len)
		{
			if ((*map)[i] && (*map)[i][j] == 1 && i != j)
			{
				!is_first ? ft_printf("%w,\n", fd) : 0;
				write_cells_json(cells, i, j, fd);
				ft_printf("%w\"value\": %d}", fd, is_path(cells, i, j, super)
					|| j == 0 ? 20 : 1);
				is_first = 0;
			}
		}
	}
	ft_printf("\n\t]\n%w}", fd);
}

void			create_visu_cells(t_cell **cells, int ***map, t_super **super)
{
	int		fd;
	int		group;
	char	*path;
	t_cell	*tmp;

	path = "./visualizer/map.json";
	if (!(fd = open(path, O_WRONLY | O_TRUNC | O_CREAT, 0666)))
		return ;
	tmp = *cells;
	ft_printf("{\n\t\"nodes\": [%w\n", fd);
	while (tmp)
	{
		group = tmp->index == 0 || tmp->index == 1 ? tmp->index : 2;
		group = group == 2 ? is_in_path(tmp, super) + 3 : group;
		ft_printf("\t\t{\"id\": \"t%s\", \"group\": %d}%w", tmp->name,
			group, fd);
		if (tmp->next)
			ft_printf(",\n%w", fd);
		else
			ft_printf("\n%w", fd);
		tmp = tmp->next;
	}
	ft_printf("\t],\n%w", fd);
	create_visu_links(cells, map, fd, super);
}
