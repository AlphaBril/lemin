/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/21 13:38:03 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/21 14:06:56 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static int	check_source_target(t_path **tmpp, t_cell **source,
	t_cell **target)
{
	while (*tmpp && (*tmpp)->next)
	{
		if ((*tmpp)->cell == *source && (*tmpp)->next->cell == *target)
			return (1);
		*tmpp = (*tmpp)->next;
	}
	return (0);
}

int			is_in_path(t_cell *cells, t_super **super)
{
	t_super		*stmp;
	t_mulpath	*tmp;
	t_path		*tmpp;

	stmp = *super;
	while (stmp)
	{
		tmp = stmp->mul;
		while (tmp)
		{
			tmpp = tmp->path;
			while (tmpp)
			{
				if (cells == tmpp->cell)
					return (tmp->index + 1);
				tmpp = tmpp->next;
			}
			tmp = tmp->next;
		}
		stmp = stmp->next;
	}
	return (0);
}

int			is_path(t_cell **cells, int i, int j, t_super **super)
{
	t_super		*stmp;
	t_cell		*source;
	t_cell		*target;
	t_mulpath	*tmp;
	t_path		*tmpp;

	source = get_cell(cells, i);
	target = get_cell(cells, j);
	stmp = *super;
	while (stmp)
	{
		tmp = stmp->mul;
		while (tmp)
		{
			tmpp = tmp->path;
			if (check_source_target(&tmpp, &source, &target) == 1)
				return (1);
			tmp = tmp->next;
		}
		stmp = stmp->next;
	}
	return (0);
}

void		write_cells_json(t_cell **cells, int i, int j, int fd)
{
	ft_printf("%w\t\t{\"source\": \"t%s\", ",
		fd, get_cell(cells, i)->name);
	ft_printf("%w\"target\": \"t%s\", ",
		fd, get_cell(cells, j)->name);
}
