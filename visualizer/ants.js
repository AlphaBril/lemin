/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants.js                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/14 09:54:57 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/24 16:13:57 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

export class Ants {
	constructor(ant_path = "ants.json") {
		this.svg = d3.select("svg");
		this.ant_path = ant_path;
	}

	createAnts(error, graph) {
		if (error) throw error;

		const delay = (amount) => {
			return new Promise((resolve) => {
				setTimeout(resolve, amount);
			});
		}

		async function loop() {
			let svg = d3.select("svg");
			let speed = 800 - (graph.ants.length * 5);
			speed = speed < 200 ? 200 : speed;
			for (let i = 0; i < graph.ants.length; i++) {
				const ant = graph.ants[i];
				ant.forEach(a => {
					var x1, x2, y1, y2;
					d3.select("#"+a.source).attr("x", (d) => {
						x1 = d.x;
						y1 = d.y;
					});
					d3.select("#"+a.target).attr("y", (d) => {
						x2 = d.x;
						y2 = d.y;
					});
					let ants = svg.append("circle")
						.attr("r", 10)
						.attr("fill", "white")
						.attr("transform", "translate(" + x1 + ", " + y1 + ")");
					ants.transition()
						.duration(speed)
						.attr("transform", "translate(" + x2 + ", " + y2 + ")");
					setTimeout(() => {
						ants.transition()
							.duration(speed - 200)
							.attr("fill", "transparent");
						ants.remove();
					}, speed);
				});
				await delay(speed);
			}
		}
		loop();
	}

	start() {
		d3.json(this.ant_path, this.createAnts.bind(this));
	}
}
