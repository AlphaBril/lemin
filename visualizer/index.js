/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index.js                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/10 11:45:03 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/23 16:40:14 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

import { Map } from './map.js';
import { Ants } from './ants.js';

Ants.prototype.callStart = function() {
	this.start();
}

Map.prototype.callHighlight = function() {
	this.highlight();
	var high = this.high;
	if (high)
	{
		document.getElementById("highI").classList.add("fa-road");
		document.getElementById("highI").classList.remove("fa-globe");
	}
	else
	{
		document.getElementById("highI").classList.add("fa-globe");
		document.getElementById("highI").classList.remove("fa-road");
	}
}

Map.prototype.pauseSimulation = function() {
	this.pause();
	var paused = this.getPauseState();
	if (paused)
	{
		document.getElementById("pauseI").classList.add("fa-play");
		document.getElementById("pauseI").classList.remove("fa-pause");
	}
	else
	{
		document.getElementById("pauseI").classList.add("fa-pause");
		document.getElementById("pauseI").classList.remove("fa-play");
	}
}

var map_path = "map.json"
var ant_path = "ants.json"
var width = 20000;
var height = 20000;

document.body.innerHTML += "<div class='mainDiv'><svg width='"+width+"' height='"+height+"'></svg></div>";
document.getElementById("startBtn").onclick = startFunc;
document.getElementById("startBtn").style.position = "fixed";
document.getElementById("startBtn").style.backgroundImage = "linear-gradient(to right, red , purple)";
document.getElementById("startBtn").style.height = "75px";
document.getElementById("startBtn").style.width = "75px";
document.getElementById("startBtn").style.bottom = "50px";
document.getElementById("startBtn").style.right = "50px";
document.getElementById("startBtn").style.borderRadius = "100%";
document.getElementById("startBtn").style.color = "white";
document.getElementById("startBtn").style.zIndex = 99;
document.getElementById("startBtn").onmouseover = hoverOn;
document.getElementById("startBtn").onmouseleave = hoverOff;
document.getElementById("startBtn").onmousedown = clickOn;
document.getElementById("startBtn").onmouseup = clickOff;

document.getElementById("highBtn").onclick = startHigh;
document.getElementById("highBtn").style.position = "fixed";
document.getElementById("highBtn").style.backgroundImage = "linear-gradient(45deg, green, yellow)";
document.getElementById("highBtn").style.height = "75px";
document.getElementById("highBtn").style.width = "75px";
document.getElementById("highBtn").style.bottom = "50px";
document.getElementById("highBtn").style.right = "150px";
document.getElementById("highBtn").style.borderRadius = "100%";
document.getElementById("highBtn").style.color = "white";
document.getElementById("highBtn").style.zIndex = 98;

document.getElementById("pauseBtn").onclick = pause;
document.getElementById("pauseBtn").style.position = "fixed";
document.getElementById("pauseBtn").style.backgroundImage = "linear-gradient(45deg, brown, gold)";
document.getElementById("pauseBtn").style.height = "75px";
document.getElementById("pauseBtn").style.width = "75px";
document.getElementById("pauseBtn").style.bottom = "50px";
document.getElementById("pauseBtn").style.right = "250px";
document.getElementById("pauseBtn").style.borderRadius = "100%";
document.getElementById("pauseBtn").style.color = "white";
document.getElementById("pauseBtn").style.zIndex = 97;

document.getElementById("strengthBar").style.position = "fixed";
document.getElementById("strengthBar").style.width = "300px";
document.getElementById("strengthBar").style.bottom = "75px";
document.getElementById("strengthBar").style.right = "500px";
document.getElementById("strengthBar").style.backgroundColor = "white";
document.getElementById("strengthBar").style.color = "white";
document.getElementById("strengthBar").style.borderColor = "white";
document.getElementById("strengthBar").style.borderWidth = "50px";
document.getElementById("strengthBar").style.zIndex = 96;

document.getElementById("pauseI").classList.add("fa-pause");
document.getElementById("startI").classList.add("fa-rocket");
document.getElementById("highI").classList.add("fa-road");

document.getElementById("startI").style.fontSize = "40px";
document.getElementById("pauseI").style.fontSize = "40px";
document.getElementById("highI").style.fontSize = "40px";


var map = new Map(map_path);

window.scroll(width / 2.3, height / 2.15);
function hoverOn() {
	document.getElementById("startBtn").style.backgroundColor = "rgba(199, 14, 14, 0.8)";
}

function hoverOff() {
	document.getElementById("startBtn").style.backgroundColor = "rgb(119, 14, 14)";
}
function clickOn() {
	document.getElementById("startBtn").style.backgroundColor = "rgba(141, 90, 73, 1)";
}

function clickOff() {
	document.getElementById("startBtn").style.backgroundColor = "rgba(199, 14, 14, 0.8)";
}

function startFunc() {
	var ants = new Ants(ant_path);
	ants.callStart();
}

function startHigh() {
	map.callHighlight();
}

function pause() {
	map.pauseSimulation();
}
