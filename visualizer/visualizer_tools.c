/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualizer_tools.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: edjubert <edjubert@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/15 12:08:44 by edjubert          #+#    #+#             */
/*   Updated: 2019/05/28 18:05:35 by edjubert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "visualizer.h"

int		open_file(char *str)
{
	int	fd;

	if (!(fd = open(str, O_WRONLY | O_TRUNC | O_CREAT, 0666)))
		return (-1);
	return (fd);
}

void	open_and_close_json(int fd, int open)
{
	if (open)
		ft_printf("%w{\n\t\"ants\": [\n", fd);
	else
		ft_printf("%w\t]\n}", fd);
}

void	initialize_ants(char ***ants, int ant_nb, char *start)
{
	int	i;

	if (!((*ants) = (char**)malloc(sizeof(char*) * ant_nb)))
		return ;
	i = -1;
	while (++i < ant_nb)
		(*ants)[i] = ft_strdup(start);
}

void	clean_ants(char ***ants, int ant_nb)
{
	int	i;

	i = -1;
	while (++i < ant_nb)
		ft_strdel(&(*ants)[i]);
	free(*ants);
}

void	clean_split(char ***split)
{
	int	i;

	i = 0;
	while ((*split)[i])
		i++;
	while ((*split)[i])
	{
		ft_strdel(&(*split)[i]);
		i--;
	}
	free(*split);
}
